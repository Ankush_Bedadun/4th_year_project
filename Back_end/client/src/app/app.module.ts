import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { BasicPage } from '../pages/segments/page';
import { TimerComponent } from '../pages/timer/timer';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { LogoutPage } from '../pages/logout/logout';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
	LoginPage,
  BasicPage,
  TimerComponent,
  TutorialPage,
  LogoutPage,
	RegisterPage

  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
	LoginPage,
  BasicPage,
  TimerComponent,
  TutorialPage,
  LogoutPage,
	RegisterPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
