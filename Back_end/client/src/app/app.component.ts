import { ViewChild,Component } from '@angular/core';
import { Platform,Nav } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { BasicPage } from '../pages/segments/page';
import { TimerComponent } from '../pages/timer/timer';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { LogoutPage } from '../pages/logout/logout';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  pages: Array<{title: string, component: any, icon: any}>

  title: string
  icon: string
  component: any
  rootPage: any = this.checkPreviousAuthorization();
  //rootPage = TutorialPage;

  constructor(private platform: Platform) {
    //platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    //});
    this.pages = [
      { title: 'Tutorial', icon: 'help-buoy', component: TutorialPage },
      { title: 'Login Page', icon: 'ios-school', component: LoginPage },
      { title: 'Sign Out', icon: 'ios-log-out', component: LogoutPage }
    ];
    this.platform = platform
      this.initializeApp()
    this.checkPreviousAuthorization()

  }
  initializeApp() {
  this.platform.ready().then(() => {
    console.log('Platform ready')
  });
}
  checkPreviousAuthorization(): any {
    console.log('checkPreviousAuthorization')
    console.log(window.localStorage.getItem('username'))
    var username = window.localStorage.getItem('username')
    if((window.localStorage.getItem('username') === "undefined" || window.localStorage.getItem('username') === null) ||
       (window.localStorage.getItem('token') === "undefined" || window.localStorage.getItem('token') === null)) {
      return TutorialPage
    } else {
      return HomePage // later change to TutorialPage
    }
  }
  openPage(page) {
    console.log(page)
    this.nav.push(page.component)

  }
}
