

export class User
{
    pwd: string;
    email: string;
    accountId: string;
    firstname: string;
    lastname: string;
}
