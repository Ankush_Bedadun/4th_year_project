import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import  'rxjs/Rx';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable'
import { User } from './user'
/*
  Generated class for the UserService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/

@Injectable()
export class UserService {
  data: any = null;
  constructor(public http: Http) {}
  public Headers;
  public RequestOptions;

  login(username,password) {
      let body = 'username='+username+'&password='+password;
      let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded', 'Accept': 'application/json' });
      let options = new RequestOptions({ headers: headers });

      return this.http.post('http://10.102.11.46:8000/api-token-auth/?format=json', body, options)
          .map(res => res.json())
          .catch(this.handleLoginError);
  }

  signup(username,password) {
      let body = 'email='+username+'&password='+password;
      let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded', 'Accept': 'application/json' });
      let options = new RequestOptions({ headers: headers });

      return this.http.post('http://10.102.11.46:8000/api/register/?format=json', body, options)
          .map(res => res.json())
          .catch(this.handleLoginError);
  }

  meditation(username,meditation) {
      let body = 'username='+username+'&meditation='+meditation;
      let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded', 'Accept': 'application/json' });
      let options = new RequestOptions({ headers: headers });

      return this.http.post('http://10.102.11.46:8000/meditation/', body, options)
          .map(res => res.json())
          .catch(this.handleLoginError);
  }
  handleLoginError(error) {

      return Observable.throw(error.json().error || 'Server error');
  }

}
