import { Component } from '@angular/core';
import { ViewController,AlertController, LoadingController,MenuController, Loading, NavController, NavParams } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { HomePage } from '../home/home';
import { User } from '../../providers/user';
import { UserService } from '../../providers/user-service';
import {TutorialPage} from '../tutorial/tutorial';
import {LogoutPage} from '../logout/logout';
/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [UserService],
})
export class LoginPage {
  loading: Loading;
  pages: Array<{title: string, component: any, icon: any}>
  title: string
  icon: string
  component: any

  registerCredentials = {email: '', password: ''};
  constructor(private nav: NavController, private _service: UserService, private params: NavParams, private viewCtrl: ViewController, public menu: MenuController, private alertCtrl: AlertController, private loadingCtrl: LoadingController,) {
    this.nav = nav
    this.menu = menu
    this.subscribeEvents()
    this.pages = [
      { title: 'Tutorial', icon: 'help-buoy', component: TutorialPage },
      //{ title: 'Class Preferences', icon: 'ios-school', component: ListPage },
      { title: 'Log Out', icon: 'ios-log-out', component: LogoutPage }
    ];
  }
  subscribeEvents() {
    console.log('listening to events on login')
    //this.events.subscribe('user:facebookSignup', () => {
      //this.facebookLogin()
    //});
  }
  public userEmailAddress: string
  public username
  public password
  formLogin() {
    if(this.username !== 'undefined' && this.password !== 'undefined') {
      this._service.login(this.username,this.password).subscribe(
          data => {
            console.log(data);
            if (data.token) {
                window.localStorage.setItem('username', this.username)
                window.localStorage.setItem('token', data.token)
                this.nav.setRoot(HomePage)
            }
          },
          err => {
            //this.showLoading();
            //this.showError("Could not log in. Please try again.");
            console.log(err);
            console.log('Could not log in. Please try again','Login Failed');
          },
          () => console.log('user login complete')
      )
    }
  }
  signupPage(){
    this.nav.push(RegisterPage);
  }
  tabsPage(){
    console.log('called tabs page')
    this.nav.setRoot(HomePage);
    window.location.reload(true)
  }
  ionViewDidEnter() {
    this.menu.enable(false);
  }
  public createAccount() {
		this.nav.push(RegisterPage);
	  }

	  public login() {

    //this.showError("please enter email");

		  //this.nav.setRoot(HomePage);
	  }

	  showLoading() {
		this.loading = this.loadingCtrl.create({
		  content: 'Please wait...'
		});
		this.loading.present();
	  }
	 showError(text) {
    setTimeout(() => {
      this.loading.dismiss();
    });

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

}
