import { Component } from '@angular/core';
import {ViewChild} from '@angular/core';
import { AlertController, LoadingController, Loading, NavController, NavParams } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { HomePage } from '../home/home';
import {TutorialPage} from '../tutorial/tutorial';
import { LogoutPage } from '../logout/logout';
import { Platform } from 'ionic-angular';
import { TimerComponent } from '../timer/timer';
import { UserService } from '../../providers/user-service';

@Component({
  selector: 'page-stats',
  templateUrl: 'page.html',
  providers: [UserService]
})
export class BasicPage {
  pet: string = "timer";
  isAndroid: boolean = false;
  @ViewChild(TimerComponent) timer: TimerComponent;
  pages: Array<{title: string, component: any, icon: any}>
  title: string
  icon: string
  component: any
  today: Date
  username : string
  meditation1 : any
  constructor(private alertCtrl: AlertController, platform: Platform, private nav: NavController, public getUser: UserService) {
    this.isAndroid = platform.is('android');
    this.username = window.localStorage.getItem('username')
    this.meditation1 = 1
    this.pages = [
      { title: 'Tutorial', icon: 'help-buoy', component: TutorialPage },
      //{ title: 'Class Preferences', icon: 'ios-school', component: ListPage },
      { title: ' Log Out', icon: 'ios-log-out', component: LogoutPage }
    ];
  }
  sendMeditation() {
    this.meditation(this.username,this.meditation1)
  }
  meditation(username: string,meditation: string){
    console.log('called meditation');
    if (username && username !== 'undefined' && meditation && meditation !== 'undefined') {
      this.getUser.meditation(this.username,this.meditation1).subscribe(
          data => {
            console.log('data from meditation');
            if (data) {
                console.log( 'got meditation data')
                window.localStorage.setItem('meditation_stats', data.meditation)
                // set first subscription to 1
                //window.localStorage.setItem('Everyone', true)

                //this.nav.push(ListPage)
                //this.nav.push(Page1)
                //this.nav.setRoot(Page1)
                //this.nav.push(Page1)
                //this.nav.push(ListPage)
                //this.nav.push(Page1)
                //this.nav.setRoot(HomePage)
                //this.nav.popToRoot()
                //this.nav.push(Page1)
                //Dialogs.alert('Meditation sessions saved successfully ','Meditation', 'Dismiss');
                this.presentAlert()
            } else {
              // failed
              if (data.error) {
                //Dialogs.alert('Could not sign up. ' + data.error + '. Try to Log In','Sign Up Failed', 'Dismiss');
              } else {
              //  Dialogs.alert('Could not sign up. try again later','Sign Up Failed', 'Dismiss');
              }
            }
          },
          err => {

            //Dialogs.alert('Could not sign up. Please try again ('+err+')','Sign Up Failed', 'Dismiss');
          },
          () => console.log('user signup complete')
      )
    } else {
      //Dialogs.alert('Must provide an email address and password','Sign Up Failed', 'Dismiss');
    }
  }
  presentAlert() {
  let alert = this.alertCtrl.create({
    title: 'Meditation Success',
    subTitle: 'Meditation Data succesfully stored.',
    buttons: ['Dismiss']
  });
  alert.present();
  }
}
