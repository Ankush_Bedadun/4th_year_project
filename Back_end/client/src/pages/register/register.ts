import { Component,OnInit,Input } from '@angular/core';
import { AlertController,NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import {LoginPage} from '../login/login';
import { UserService } from '../../providers/user-service';
import { User } from '../../providers/user';
import {Dialogs} from 'ionic-native'
/*
  Generated class for the Register page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
  providers: [UserService]
})
export class RegisterPage {
  @Input() user:User;
     responseStatus:Object= [];
     status:boolean ;
  constructor(private nav: NavController, private alertCtrl: AlertController ,public navParams: NavParams,public _service: UserService) {
      this.nav = nav

  }
  public token;
  public json;
  public username;
  public password;
    email: '';
     pwd: '';
     accountId: '';
     firstname: '' ;
  public register() {

     //this.showPopup("Success", "Account created.");
     //this.showPopup("Error", "Problem creating account.");

      //this.showPopup("Error", error);
	}
  formSignup() {
    this.signup(this.username,this.password)
  }
  signup(username: string,password: string){
    console.log('called signup');
    if (username && username !== 'undefined' && password && password !== 'undefined') {
      this._service.signup(this.username,this.password).subscribe(
          data => {
            console.log('data from signup');
            if (data.token) {
                console.log('signup in')
                window.localStorage.setItem('username', this.username)
                window.localStorage.setItem('token', data.token)
                // set first subscription to 1
                //window.localStorage.setItem('Everyone', true)

                //this.nav.push(ListPage)
                //this.nav.push(Page1)
                //this.nav.setRoot(Page1)
                //this.nav.push(Page1)
                //this.nav.push(ListPage)
                //this.nav.push(Page1)
                this.nav.setRoot(HomePage)
                //this.nav.popToRoot()
                //this.nav.push(Page1)


            } else {
              // failed
              if (data.error) {
                Dialogs.alert('Could not sign up. ' + data.error + '. Try to Log In','Sign Up Failed', 'Dismiss');
              } else {
                Dialogs.alert('Could not sign up. try again later','Sign Up Failed', 'Dismiss');
              }
            }
          },
          err => {

            Dialogs.alert('Could not sign up. Please try again ('+err+')','Sign Up Failed', 'Dismiss');
          },
          () => console.log('user signup complete')
      )
    } else {
      Dialogs.alert('Must provide an email address and password','Sign Up Failed', 'Dismiss');
    }
  }
  loginPage(){
  	this.nav.push(LoginPage);
  }

  ngOnInit() {

  }
}
