import { Component, ViewChild} from '@angular/core';

import { NavController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import {TutorialPage} from '../tutorial/tutorial';
import { LogoutPage } from '../logout/logout';
import { UserService } from '../../providers/user-service';
import { BasicPage } from '../segments/page';
import { TimerComponent } from '../timer/timer';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [UserService]
})
export class HomePage {
  dataPage = BasicPage;
  timerPage = TimerComponent;

  pages: Array<{title: string, component: any, icon: any}>
  title: string
  icon: string
  component: any
  today: Date
  username : string
  public recvData:any;
  constructor(public getUser: UserService, private nav: NavController,) {
    //  this.loadUser();
    //this.listenToEvents()
    //this.today = new Date()
    this.username = window.localStorage.getItem('username')
    this.pages = [
      { title: 'Tutorial', icon: 'help-buoy', component: TutorialPage },
      //{ title: 'Class Preferences', icon: 'ios-school', component: ListPage },
      { title: ' Log Out', icon: 'ios-log-out', component: LogoutPage }
    ];
  }
  //loadUser(){
  //  this.getUser.load()
  //  .then(data => {
  //    this.recvData = data;
  //    console.log("the received data is >> ",  this.recvData);
  //  });


  //}
	public logout() {
        this.nav.setRoot(LoginPage)
  }

}
