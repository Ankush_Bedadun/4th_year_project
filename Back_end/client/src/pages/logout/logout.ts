import { Component,OnInit,Input } from '@angular/core';
import { NavController} from 'ionic-angular';
import {LoginPage} from '../login/login';
import { UserService } from '../../providers/user-service';
import {getRootNav} from '../../utils/navUtils';

@Component({
  templateUrl: 'logout.html',
  providers: [UserService]
})
export class LogoutPage {

  constructor(public nav: NavController) {
    this.nav = nav
  }

  ngOnInit() {
    console.log('userLogout')
    window.localStorage.removeItem('username')
    window.localStorage.removeItem('token')
    this.nav.setRoot(LoginPage)
    //let rootNav = getRootNav(this.navCtrl);
    //rootNav.setRoot(LoginPage);
    //rootNav.popToRoot();
    //let rootNav = getRootNav(this.navCtrl);
    //rootNav.setRoot(LoginPage);
    //rootNav.popToRoot();
  }
}
