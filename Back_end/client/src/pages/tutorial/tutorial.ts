import {NavController, MenuController} from 'ionic-angular';
import {Component, Input} from '@angular/core';
//import {LoginPage} from '../login/login';
//import {Page1} from '../page1/page1';

import {LogoutPage} from '../logout/logout';
import {LoginPage} from '../login/login';
/*
  Generated class for the TutorialPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html',
})
export class TutorialPage {

  public backimg

  pages: Array<{title: string, component: any, icon: any}>
  title: string
  icon: string
  component: any

  constructor(public nav: NavController, public menu: MenuController) {
    this.nav = nav;
    this.menu = menu;

    this.backimg = 'img/background_login.png';
    this.pages = [
      { title: 'Tutorial', icon: 'help-buoy', component: TutorialPage },
      { title: 'Class Preferences', icon: 'ios-school', component: LoginPage },
      { title: 'Log Out', icon: 'ios-log-out', component: LogoutPage }
    ];
    this.slides = [
      {
        title: "Welcome to Mindspace",
        description: "<b>Mindspace</b> is a meditation app with twist.",
        image: "img/logo_white.png",
      },
      {
        title: "What's Inside?",
        description: "Inside, you will have the option to register and track your meditation sessions as well as tracking your emotional health. ",
        image: "img/logo_white.png",
      },
      {
        title: "How to use?",
        description: "Create your account and login which will give you the options such as use the meditation timer, the emotional journal or even try different meditation techniques for all levels.",
        image: "img/logo_white.png",
      }
    ];
  }

  public slides;
  public showSkip;

  startApp() {
  	if((window.localStorage.getItem('username') === "undefined" || window.localStorage.getItem('username') === null) &&
       (window.localStorage.getItem('token') === "undefined" || window.localStorage.getItem('token') === null)) {
  		this.nav.setRoot(LoginPage);
  	} else {
    	this.nav.setRoot(LoginPage);
    }
  }

  onSlideChangeStart(slider) {
    this.showSkip = !slider.isEnd;
  }

  ionViewDidEnter() {
    // the left menu should be disabled on the tutorial page
    this.menu.enable(false);
  }

  ionViewDidLeave() {
    // enable the left menu when leaving the tutorial page
    this.menu.enable(true);
  }
}
