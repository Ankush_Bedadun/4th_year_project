from django.db import models
from django.utils import timezone
from django.core.validators import MaxValueValidator, MinValueValidator


class User(models.Model):
    """User model"""
    accountId = models.CharField(max_length=36)
    email = models.CharField(max_length=130)
    pwd = models.CharField(max_length=36)
    firstname = models.CharField(max_length=50)
    lastname = models.CharField(max_length=50)
