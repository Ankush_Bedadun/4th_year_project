from rest_framework import serializers
from rest_framework.response import Response
from django.contrib.auth.models import User
from models import Meditation,Emotion

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username']
class MeditationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Meditation
        fields = ['username', 'meditation','date']
class EmotionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Emotion
        fields = ['username', 'emotion','date']
