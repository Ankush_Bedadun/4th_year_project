from rest_framework import viewsets,permissions
from rest_framework.decorators import detail_route, list_route
from django.views.decorators.csrf import csrf_exempt
from rest_framework_jwt.settings import api_settings
from django.http import HttpResponse
from django.contrib.auth.models import User
from user.serializers import UserSerializer, MeditationSerializer
from models import Meditation
from django.dispatch import receiver
from django.db.models.signals import post_save


from rest_framework.authtoken.models import Token
from django.views.decorators.csrf import csrf_exempt
from rest_framework_jwt.settings import api_settings

from django.core.exceptions import ValidationError
from django.core.validators import validate_email

import datetime

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

class MeditationViewSet(viewsets.ModelViewSet):
    queryset = Meditation.objects.all()
    serializer_class = MeditationSerializer

@csrf_exempt
def create_meditation(request):

	if request.POST.get('username') and request.POST.get('meditation'):

		try:
			meditation = Meditation.objects.get(username=request.POST.get('username').replace(' ',''))
		except User.DoesNotExist:
			user = None
    		if user:
    			print user
    			return HttpResponse('{"error":"username already exists"}', content_type="application/json")
    		else:
    			user = User.objects.create_user(
    				request.POST.get('email'),
    				request.POST.get('email'),
    				request.POST.get('password')
    			)
    			print "this has passed2"
    			jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    			jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
    			payload = jwt_payload_handler(user)
    			token = jwt_encode_handler(payload)
    			print "this has passed3"
    			return HttpResponse('{"token":"'+token+'"}', content_type="application/json")
	else:
		return HttpResponse('{"error":"missing parameters, could not create user"}', content_type="application/json")

"""
API Create new user
"""
@csrf_exempt
def create_auth(request):

	if request.POST.get('email') and request.POST.get('password'):

		try:
			validate_email(request.POST.get('email'))
		except ValidationError as e:
			return HttpResponse('{"error":"email address is not valid"}', content_type="application/json")
		else:
			print "email check passed", request.POST.get('email')


		try:
			user = User.objects.get(username=request.POST.get('email').replace(' ',''))
		except User.DoesNotExist:
			user = None
    		if user:
    			print user
    			return HttpResponse('{"error":"username already exists"}', content_type="application/json")
    		else:
    			user = User.objects.create_user(
    				request.POST.get('email'),
    				request.POST.get('email'),
    				request.POST.get('password')
    			)
    			print "this has passed2"
    			jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    			jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
    			payload = jwt_payload_handler(user)
    			token = jwt_encode_handler(payload)
    			print "this has passed3"
    			return HttpResponse('{"token":"'+token+'"}', content_type="application/json")
	else:
		return HttpResponse('{"error":"missing parameters, could not create user"}', content_type="application/json")
