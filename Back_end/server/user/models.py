from django.db import models
from django.utils import timezone
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import User

class Meditation(models.Model):
    username = models.EmailField(max_length=100)
    meditation = models.IntegerField(default='0')
    date = models.DateField(default = datetime.date.today)

class Emotion(models.Model):
    username = models.EmailField(max_length=100)
    emotion = models.CharField(max_length=100)
    date = models.DateField(default = datetime.date.today)
