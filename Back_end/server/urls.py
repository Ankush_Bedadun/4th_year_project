"""Back_end URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
#from django.conf.urls import url
#from django.contrib import admin

#urlpatterns = [
#    url(r'^admin/', admin.site.urls),
#]
from rest_framework.routers import DefaultRouter
from user.views import UserViewSet,create_auth,MeditationViewSet
from django.conf.urls import include,url
from rest_framework_jwt.views import obtain_jwt_token

router = DefaultRouter()
router.register(prefix='users', viewset=UserViewSet)
router.register(prefix='meditation', viewset=MeditationViewSet)
#urlpatterns = router.urls
urlpatterns = [
     url(r'^', include(router.urls)),
     url(r'^api-token-auth/', obtain_jwt_token),
     url(r'^api/register', create_auth),

]
